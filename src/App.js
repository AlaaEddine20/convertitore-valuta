import React from "react";
import Home from "./components/home/Home";
import AboutUs from "./components/about_us/AboutUs";
import ContactUs from "./components/contact_us/ContactUs";
import Navbar from "./components/navbar/Navbar";
import { Redirect, Route, Switch, useLocation } from "react-router-dom";
import { AnimatePresence } from "framer-motion";
import "./App.css";

const App = () => {
  const location = useLocation();
  return (
    <>
      <Navbar />
      <div className="App" style={{ overflow: "hidden" }}>
        <AnimatePresence exitBeforeEnter>
          <Switch location={location} key={location.pathname}>
            <Route exact path="/">
              <Redirect to="/home" />
            </Route>
            <Route exact path="/home">
              <Home />
            </Route>
            <Route exact path="/about">
              <AboutUs />
            </Route>
            <Route exact path="/contact">
              <ContactUs />
            </Route>
          </Switch>
        </AnimatePresence>
      </div>
    </>
  );
};

export default App;
