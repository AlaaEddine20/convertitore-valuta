import React, { useEffect, useState } from "react";
import axios from "axios";
import Converter from "./../converter/Converter";
import { CircularProgress } from "@material-ui/core";

const url = `http://data.fixer.io/api`;

const Home = () => {
  const [currency1, setCurrency1] = useState([]);
  const [currency2, setCurrency2] = useState([]);
  const [date, setDate] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    setIsLoading(true);
    try {
      const res = await axios.get(
        `${url}/latest?access_key=${process.env.REACT_APP_API_KEY}`,
      );
      setDate(res.data.date);
      setIsLoading(false);
      setCurrency1(res.data.rates);
      setCurrency2(res.data.rates);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {isLoading ? (
        <div>
          <CircularProgress color="secondary" />
        </div>
      ) : (
        <Converter
          isLoading={isLoading}
          date={date}
          currency1={currency1}
          currency2={currency2}
        />
      )}
    </>
  );
};

export default Home;
