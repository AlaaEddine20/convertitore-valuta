import React, { useState } from "react";
import { Button, TextField } from "@material-ui/core";
import { motion } from "framer-motion";
import styles from "./Styles.module.scss";
import axios from "axios";

const initialValues = {
  name: "",
  email: "",
  message: "",
};

const pageTransitions = {
  in: {
    opacity: 1,
    y: 0,
  },
  out: {
    opacity: 0,
    y: "-100vh",
  },
};

const ContactUs = () => {
  const [values, setValues] = useState(initialValues);
  const [sending, setSending] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setSending(true);

    const body = {
      values,
    };

    await axios.post("https://hookb.in/2qj3M9m6KOU9BBKGpyog", body);
    setSending(false);
  };

  return (
    <motion.div
      initial="out"
      animate="in"
      exit="out"
      variants={pageTransitions}
      className={styles.container}
    >
      <h1 style={{ textAlign: "center" }}>Contattaci</h1>
      <form onSubmit={handleSubmit} className={styles.form_container}>
        <TextField
          value={values.name}
          onChange={handleInputChange}
          name="name"
          label="Nome"
          margin="normal"
        ></TextField>
        <TextField
          value={values.email}
          onChange={handleInputChange}
          name="email"
          label="Indirizzo Email"
          margin="normal"
        ></TextField>
        <TextField
          value={values.message}
          onChange={handleInputChange}
          name="message"
          label="Il tuo messaggio"
          multiline
          margin="normal"
          rows={4}
        ></TextField>
        <Button type="submit" variant="contained" color="primary">
          {sending ? "Invio in corso..." : "Invia"}
        </Button>
      </form>
    </motion.div>
  );
};

export default ContactUs;
