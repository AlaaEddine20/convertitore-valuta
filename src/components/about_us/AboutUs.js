import React from "react";
import { motion } from "framer-motion";
import styles from "./Styles.module.scss";

const pageTransitions = {
  in: {
    opacity: 1,
    y: 0,
  },
  out: {
    opacity: 0,
    y: "-100vh",
  },
};

const AboutUs = () => {
  return (
    <motion.div
      initial="out"
      animate="in"
      exit="out"
      variants={pageTransitions}
      className={styles.container}
    >
      <h1> Chi Siamo </h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in purus
        egestas, placerat ex quis, euismod enim. Donec sed interdum diam, a
        accumsan ipsum. Phasellus dictum mauris et eros molestie, et euismod
        lorem posuere. Proin in bibendum est. Praesent a tempus ligula. Sed
        ultricies molestie est, in dapibus sapien elementum sit amet. In porta
        mi sed mauris cursus ultricies. Sed scelerisque hendrerit erat, blandit
        pretium urna. Morbi ultrices magna enim, non sollicitudin enim semper
        non. Mauris bibendum mi sed ante faucibus lacinia.
      </p>
    </motion.div>
  );
};

export default AboutUs;
