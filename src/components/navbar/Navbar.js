import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import { NavLink } from "react-router-dom";
import styles from "./Syles.module.scss";

const Navbar = () => {
  return (
    <>
      <AppBar className={styles.navbar_container} position="static">
        <Toolbar className={styles.nav_links}>
          <NavLink
            to="/home"
            activeStyle={{ backgroundColor: "#0096c7" }}
            style={{ textDecoration: "none", color: "#edf2f4" }}
          >
            <Typography variant="h6">Home</Typography>
          </NavLink>
          <NavLink
            to="/about"
            activeStyle={{ backgroundColor: "#0096c7" }}
            style={{ textDecoration: "none", color: "#edf2f4" }}
          >
            <Typography variant="h6">Chi Siamo</Typography>
          </NavLink>
          <NavLink
            activeStyle={{ backgroundColor: "#0096c7" }}
            to="/contact"
            style={{ textDecoration: "none", color: "#edf2f4" }}
          >
            <Typography variant="h6">Contattaci</Typography>
          </NavLink>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Navbar;
