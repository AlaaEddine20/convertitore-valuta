import React, { useState } from "react";
import { TextField, Select, Typography, Button } from "@material-ui/core";
import { motion } from "framer-motion";
import styles from "./Styles.module.scss";

const pageTransitions = {
  in: {
    opacity: 1,
    y: 0,
  },
  out: {
    opacity: 0,
    y: "-100vh",
  },
};

const Converter = ({ currency1, date, currency2 }) => {
  const [inputFrom, setInputFrom] = useState(0);
  const [inputTo, setInputTo] = useState(0);
  const [selectedFrom, setSelectedFrom] = useState("");
  const [selectedTo, setSelectedTo] = useState("");

  const handleFromInput = (e) => {
    setInputFrom(e.target.value);
  };

  const handleToInput = (e) => {
    setInputTo(e.target.value);
  };

  const handleSelectedFrom = (e) => {
    setSelectedFrom(e.target.value);
  };

  const handleSelectedTo = (e) => {
    setSelectedTo(e.target.value);
  };

  const convert = (e) => {
    e.preventDefault();
    let result = (selectedTo / selectedFrom) * inputFrom;
    setInputTo(result.toFixed(4));
  };

  return (
    <>
      <motion.div
        initial="out"
        animate="in"
        exit="out"
        variants={pageTransitions}
        className={styles.container}
      >
        <Typography variant="h4">Currency converter</Typography>
        <form onSubmit={convert}>
          <div>
            <TextField
              onChange={handleFromInput}
              value={inputFrom}
              color="primary"
              type="number"
              variant="outlined"
              label="Da"
            />
            <Select native value={selectedFrom} onChange={handleSelectedFrom}>
              {Object.keys(currency1).map((value) => (
                <option key={value} value={currency1[value]}>
                  {value}
                </option>
              ))}
            </Select>
          </div>
          <div>
            <TextField
              onChange={handleToInput}
              value={inputTo === null ? "Calcutating" : inputTo}
              disabled
              variant="outlined"
              label="a"
            />
            <Select native value={selectedTo} onChange={handleSelectedTo}>
              {Object.keys(currency2).map((value) => (
                <option key={value} value={currency2[value]}>
                  {value}
                </option>
              ))}
            </Select>
          </div>
          <p>
            Tasso di cambio è <strong>{inputTo}</strong> al: {date}
          </p>
          <Button color="primary" variant="contained" type="submit">
            Convert
          </Button>
        </form>
      </motion.div>
    </>
  );
};

export default Converter;
